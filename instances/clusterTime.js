'use strict';

module.exports = {

	// SQL
	_s: {

		UPDATE:'UPDATE `game.clusters` SET year = year + 0.1'

	},

	rollback:function(){

		let that = this;
		return new Promise((resolve,reject)=>{

			global._s.db.q('ROLLBACK;',[])
				.then(()=>{	

					global._s.log('! rollback successful','I');
					resolve();
				
				})
				.catch(e=>{

					global._s.log('x rollback error!','E');					
					global._s.log(e,'E',true);
					reject(e);

				});

		});

	},

	commit:function() {

		let that = this;
		return new Promise((resolve,reject)=>{
					
			global._s.log('. committing','D');
			global._s.db.q('COMMIT;',[])
				.then(()=>{

					global._s.log('✔ commit successful','D');
					resolve();

				})
				.catch(e=>{

					global._s.log('x error committing transaction','E');
					global._s.log(e,'E',true);
					
					that.rollback()
						.then(()=>{

							resolve();
						
						})
						.catch(e=>{
							
							global._s.log('x rollback error!','E');					
							global._s.log(e,'E',true);

							reject();

						});

				});

		});

	},

	// initialization promise 
	init:function(options){

		let that = this;
		return new Promise((resolve,reject)=>{

			global._s.log('✔ Running Cluster Time','D');

			let localInterval = setInterval(()=>{

				global._s.db.q('START TRANSACTION;',[])
					.then(()=>{

						global._s.db.q(that._s.UPDATE,[])
							.then(res=>{

								global._s.log('✔ Cluster Time Update Successful','I');
								that.commit();

							})
							.catch(e=>{

								global._s.log('x Cluster Time Update Error','E');
								global._s.log(e,'E',true);

								that.rollback();

							});

					})
					.catch(e=>{

						global._s.log('x Cluster Time Update Error: starting transaction','E');
						global._s.log(e,'E',true);

					});

			},options.interval); // interval

			resolve(localInterval);

		}); // promise

	}

};