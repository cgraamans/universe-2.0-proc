'use strict';

module.exports = {

	host:'http://localhost:9001',
	auth:{
		name:process.env._WORKER_ID,
		password:process.env._WORKER_TOKEN,
	},
	// isConnected:false,
	// retries:100,
	// retryTimeout:500,
	// connectionPoll:6000, // ms

};
