'use strict';

module.exports = {
	
	settings:{
		user:process.env._DB_USERNAME,
		password:process.env._DB_PASSWD,
		database:process.env._DB,
		host:'localhost',
		charset:'utf8mb4',
		multipleStatements: true,
	},
	retries:100,
		
};